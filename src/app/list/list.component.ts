import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../entity/product';
import { Router } from '@angular/router';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';

const deleteProduct = gql`
  mutation removeProduct($id: String!) {
    removeProduct(id: $id) {
      _id
    }
  }
`;

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  // public dataSource: Product[];
  displayedColumns: string[] = ['_id', 'name', 'price', 'actions'];

  dataSource: Product[] = [];
  private query: QueryRef<any>;
  resp: any = {};

  constructor(private productService: ProductService, private router: Router, private apollo: Apollo) { }

  getProductList() {
    this.apollo.query({
      query: gql `{ products { _id, name, price } }`
    }).subscribe(res => {
      this.resp = res;
      this.dataSource = this.resp.data.products;
    });
  }

  ngOnInit() {
    // this.productService.getProductList().subscribe((products) => {
    //   this.dataSource = products;
    // });

    this.getProductList();
  }

  edit(product: Product) {
    this.router.navigate(['/edit', product._id]);
  }

  delete(product: Product) {
    // this.productService.deleteProduct(product._id).subscribe(
    //   () => {
    //     this.productService.getProductList().subscribe((products) => {
    //       this.dataSource = products;
    //     });
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );

    this.apollo.mutate({
      mutation: deleteProduct,
      variables: {
        id: product._id
      }
    }).subscribe(() => {
      this.getProductList();
    }, (error) => {
      console.log(error);
    });
  }
}
