import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../entity/product';
import { ProductService } from '../product.service';
import { Apollo, QueryRef } from 'apollo-angular';
import gql from 'graphql-tag';

const productQuery = gql`
  query product($productId: String) {
    product(id: $productId) {
      _id
      name
      price
    }
  }
`;

const updateProduct = gql`
  mutation updateProduct(
    $id: String!,
    $name: String!,
    $price: Int!) {
    updateProduct(
      id: $id,
      name: $name,
      price: $price) {
        _id
        name
        price
    }
  }
`;

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {

  id: number;
  product: Product = new Product();
  resp: any = {};

  constructor(
    private productService: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private apollo: Apollo
  ) {
    this.id = this.activatedRoute.snapshot.params['id'];
  }

  getProductDetails() {
    this.apollo.query({
      query: productQuery,
      variables: {productId: this.id}
    }).subscribe(res => {
      this.resp = res;
      this.product = this.resp.data.product;
    });
  }

  ngOnInit() {
    // this.productService.getProduct(this.id).subscribe(
    //   data => {
    //     this.product = data;
    //   },
    //   error => {
    //     console.log(error);
    //   }
    // );
    this.getProductDetails();
  }

  submit() {
    // this.productService.updateProduct(this.product).subscribe(
    //   () => {
    //     this.router.navigate(['/list']);
    //   },
    //   error => {
    //     console.log(error);
    //   });

    this.apollo.mutate({
      mutation: updateProduct,
      variables: {
        id: this.product._id,
        name: this.product.name,
        price: this.product.price
      }
    }).subscribe(({ data }) => {
      this.router.navigate(['/list']);
    }, (error) => {
      console.log(error);
    });
  }

}
