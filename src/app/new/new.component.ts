import { Component, OnInit } from '@angular/core';
import { ProductService } from '../product.service';
import { Product } from '../entity/product';
import { Router } from '@angular/router';
import { Apollo } from 'apollo-angular';
import gql from 'graphql-tag';

const addProduct = gql`
  mutation addProduct($name: String!, $price: Int!) {
    addProduct(name: $name, price: $price) {
      _id
    }
  }
`;

@Component({
  selector: 'app-new',
  templateUrl: './new.component.html',
  styleUrls: ['./new.component.css']
})
export class NewComponent implements OnInit {

  product: Product = new Product();

  constructor(private productService: ProductService, private router: Router, private apollo: Apollo) { }

  ngOnInit() {
  }

  submit() {
    // this.productService.newProduct(this.product).subscribe(
    //   () => {
    //     this.router.navigate(['/list']);
    //   },
    //   error => {
    //     console.log(error);
    //   });

    this.apollo.mutate({
      mutation: addProduct,
      variables: {
        name: this.product.name,
        price: this.product.price
      }
    }).subscribe((data) => {
      this.router.navigate(['/list']);
    }, (error) => {
      console.log(error);
    });
  }

}
