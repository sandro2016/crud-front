import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from './entity/product';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http: HttpClient) { }

  baseUrl = environment.baseUrl;

  public getProductList(): Observable<any> {
    return this.http.get(this.baseUrl);
  }

  public getProduct(id: number): Observable<any> {
    return this.http.get(this.baseUrl + '/' + id);
  }

  public newProduct(product: Product): Observable<any> {
    return this.http.post(this.baseUrl + '/create', product);
  }

  public updateProduct(product: Product): Observable<any> {
    return this.http.put(this.baseUrl + '/update', product);
  }

  public deleteProduct(id: string): Observable<any> {
    return this.http.delete(this.baseUrl + '/delete/' + id);
  }
}
